﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class destroyByContact : MonoBehaviour 

	

{
	public GameObject explosion;
	public int scoreValue;
	private GameController gameController;
	public int startHealth;
	public int health;


	


	void Start ()
	{

		health = startHealth;

		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {

			gameController = gameControllerObject.GetComponent <GameController>();
		}
		else
		{

			Debug.Log ("NaN");
		}
	}


	void OnTriggerEnter(Collider other){
		if (other.tag == "Asteroid") {
			health -= 1;
			if (health <= 0) {
				Instantiate(explosion, transform.position, transform.rotation);
				Destroy (gameObject);
			}
		}



		if (other.tag == "Player") {
			health -= 1;
			if (health <= 0) {
				Instantiate(explosion, transform.position, transform.rotation);
				Destroy (gameObject);
			}


		}
		if (other.tag == "Bolt") {
			health -= 3;
			if (health <= 0) {
				Instantiate(explosion, transform.position, transform.rotation);
				Destroy (gameObject);
			}
		}




}
}
