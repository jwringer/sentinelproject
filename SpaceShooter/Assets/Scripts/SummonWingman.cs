﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SummonWingman : MonoBehaviour {

	private GameController gameController;

	public bool summoning = false;
	public GameObject wingman;
	private int score;

	void Start(){

		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {

			gameController = gameControllerObject.GetComponent <GameController>();
		}

		score = gameController.score;

	}



	public void ButtonInteract()

	{
		
		score = gameController.score;

		if (score >= 100) {
			summoning = true;
		}

	}
}
