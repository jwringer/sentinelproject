﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour {

	private GameObject player;
	private bool shielding;
	private ShieldScript shieldingScript;
	public GameObject shieldScriptObject;

	// Use this for initialization
	void Start () {
		shieldScriptObject = GameObject.FindWithTag("MainCamera");
		shieldingScript = shieldScriptObject.GetComponent<ShieldScript> ();

	}
	
	// Update is called once per frame
	void Update () {
		
		player = GameObject.FindWithTag ("Player");
		Vector3 playerLocation = player.transform.position;
		transform.position = player.transform.position;
	}

void OnTriggerEnter(Collider other){
	if (other.tag == "Asteroid") {
			shieldingScript.shielded = false;
			Destroy (other.gameObject);
			Destroy (gameObject);

		}
	}

}