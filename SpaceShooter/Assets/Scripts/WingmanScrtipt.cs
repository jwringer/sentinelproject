﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingmanScrtipt : MonoBehaviour {



	public GameObject shot;
	public GameObject shotSpawn;
	public float fireRate;
	public float delay;

	void Start ()
	{

		InvokeRepeating ("Fire", delay, fireRate);
	}

	void Fire ()
	{
		Vector3 spawnPosition = shotSpawn.transform.position;
		spawnPosition.z += 1;
		Instantiate(shot, spawnPosition, shotSpawn.transform.rotation);
		GetComponent<AudioSource>().Play();
	}
}