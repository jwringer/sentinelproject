﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldScript : MonoBehaviour {

	private GameController gameController;

	public bool shielded = false;
	public GameObject shield;
	private int score;
	public GameObject player;


	void Start(){

		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {

			gameController = gameControllerObject.GetComponent <GameController>();
		}

		score = gameController.score;

	}



	public void ButtonInteract()

	{
		score = gameController.score;

	

		if (score >= 50) 
			
			if (shielded == false) {
				shielded = true;
				Instantiate(shield, player.transform.position, player.transform.rotation);
				gameController.AddScore (-50);

			}
		}



	}

