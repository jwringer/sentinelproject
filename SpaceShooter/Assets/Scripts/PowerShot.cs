﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerShot : MonoBehaviour {

	private GameController gameController;

	public GameObject powerShot;
	private int score;
	public GameObject player;
	// Use this for initialization
	void Start () {
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {

			gameController = gameControllerObject.GetComponent <GameController>();
		}

		score = gameController.score;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ButtonInteract()

	{

		score = gameController.score;

		if (score >= 75) {
			Instantiate(powerShot, player.transform.position, player.transform.rotation);
			gameController.AddScore (-75);
		}

	}
}
