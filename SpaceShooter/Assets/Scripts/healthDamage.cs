﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class healthDamage : MonoBehaviour 



{
	public GameObject explosion;
	public int scoreValue;
	private GameController gameController;
	public int startHealth;
	private int health;





	void Start ()
	{

		health = startHealth;

		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {

			gameController = gameControllerObject.GetComponent <GameController>();
		}
		else
		{

			Debug.Log ("NaN");
		}
	}


	void OnTriggerEnter(Collider other){
		if (other.tag == "Asteroid") {
			health -= 1;
			if (health <= 0) {
				Instantiate(explosion, transform.position, transform.rotation);
				Destroy (gameObject);
				gameController.AddScore (scoreValue);

			}
		}



		if (other.tag == "Player") {
			health -= 1;
			if (health <= 0) {
				Instantiate(explosion, transform.position, transform.rotation);
				Destroy (gameObject);
				gameController.AddScore (scoreValue);
			}


		}
		if (other.tag == "Bolt") {
			health -= 3;
			if (health <= 0) {
				Instantiate(explosion, transform.position, transform.rotation);
				Destroy (gameObject);
				gameController.AddScore (scoreValue);
			}
		}
		if (other.tag == "Border") {
			health -= 10000;
			if (health <= 0) {
				Instantiate(explosion, transform.position, transform.rotation);
				Destroy (gameObject);
				gameController.AddScore (scoreValue);
			}
		}



	}
}
