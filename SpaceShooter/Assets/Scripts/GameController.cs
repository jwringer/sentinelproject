﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour 
{

	public GameObject hazard;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;


	public GameObject summonObject;
	private SummonWingman summonScript;
	bool boolSummon;

	private GameObject player;

	public GUIText scoreText;
	public GUIText restartText;
	public GUIText gameOverText;

	private bool gameOver;
	private bool restart;
	public int score;

	public GameObject placeText;

	public GameObject wingman;

	void Start()
	{

		summonScript = summonObject.GetComponent<SummonWingman> ();


		placeText.SetActive (false);
		restart = false;
		gameOver = false;
		restartText.text = "";
		gameOverText.text = "";
		score = 0;
		updateScore();
		StartCoroutine(SpawnWaves ());
	}

	void Update()
	{



		boolSummon = summonScript.summoning;




		if (boolSummon == true) {
			Time.timeScale = 0.0F;
			placeText.SetActive (true);
			if (Input.GetMouseButtonDown (0)) {
				Vector3 spawnPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				spawnPosition.y = 0.0f;

				GameObject objectInstance = Instantiate (wingman, spawnPosition, Quaternion.Euler (new Vector3 (0, 0, 0)));
				AddScore (-100);
				summonScript.summoning = false;

			}

		} 
		else {
			Time.timeScale = 1.0F;
			placeText.SetActive (false);
		}



		if (this.restart && Input.GetKeyDown(KeyCode.R))
		{
			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		}


		GameObject player = GameObject.FindWithTag ("Player");
		if (player == null) {

			gameOver = true;
		}
	}



	IEnumerator SpawnWaves ()

	{
		yield return new WaitForSeconds (startWait);
		while (true) 
		{
			
			for (int i = 0; i < hazardCount; i++) 
			{
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);

			}
			yield return new WaitForSeconds (waveWait);


			if (gameOver) {
				restartText.text = "Press 'R' for Restart";
				restart = true;
				break;
			}

		}

	}



	public void GameOver ()
	{
		gameOverText.text = "Game Over!";
		gameOver = true;
	}

	public void AddScore (int newScoreValue)
	{

		score += newScoreValue;
		updateScore ();

	}


	void updateScore()
	{

		scoreText.text = "Resources: " + score;

	}




}



